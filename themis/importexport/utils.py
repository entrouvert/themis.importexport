import datetime

from zope import component
from Products.CMFCore.WorkflowCore import WorkflowException
from Products.Five.browser import BrowserView
from Products.CMFCore.utils import getToolByName
from zope.app.intid.interfaces import IIntIds
from plone.registry.interfaces import IRegistry
from tabellio.config.interfaces import ITabellioSettings

import plone.namedfile.field
import z3c.relationfield.relation

class UtilityView(BrowserView):

    def get_folder_at_path(self, path):
        current = self.portal
        for part in path.split('/'):
            if not part:
                continue
            current = getattr(current, part)
        return current

    _deputies_folder = None
    def deputies_folder(self):
        if self._deputies_folder:
            return self._deputies_folder
        path = self.settings.deputiesPath
        self._deputies_folder = self.get_folder_at_path(path)
        return self._deputies_folder
    deputies_folder = property(deputies_folder)

    _commissions_folder = None
    def commissions_folder(self):
        if self._commissions_folder:
            return self._commissions_folder
        path = self.settings.commissionsPath
        self._commissions_folder = self.get_folder_at_path(path)
        return self._commissions_folder
    commissions_folder = property(commissions_folder)

    _documents_folder = None
    def documents_folder(self):
        if self._documents_folder:
            return self._documents_folder
        path = self.settings.documentsPath
        self._documents_folder = self.get_folder_at_path(path)
        return self._documents_folder
    documents_folder = property(documents_folder)

    _questions_folder = None
    def questions_folder(self):
        if self._questions_folder:
            return self._questions_folder
        path = self.settings.questionsPath
        self._questions_folder = self.get_folder_at_path(path)
        return self._questions_folder
    questions_folder = property(questions_folder)

    _convocations_folder = None
    def convocations_folder(self):
        if self._convocations_folder:
            return self._convocations_folder
        path = self.settings.convocationsPath
        self._convocations_folder = self.get_folder_at_path(path)
        return self._convocations_folder
    convocations_folder = property(convocations_folder)

    _polgroups_folder = None
    def polgroups_folder(self):
        if self._polgroups_folder:
            return self._polgroups_folder
        path = self.settings.polgroupsPath
        self._polgroups_folder = self.get_folder_at_path(path)
        return self._polgroups_folder
    polgroups_folder = property(polgroups_folder)

    _polgroups_intids = None
    def get_polgroup_intid(self, title):
        if not self._polgroups_intids:
            self._polgroups_intids = {}

        if title in self._polgroups_intids:
            return self._polgroups_intids.get(title)

        polgroup_id = self.plone_utils.normalizeString(title)
        if not hasattr(self.polgroups_folder, polgroup_id):
            self.polgroups_folder.invokeFactory('themis.datatypes.polgroup', polgroup_id, title=title)
            try:
                self.portal_workflow.doActionFor(getattr(self.polgroups_folder, polgroup_id), 'publish')
            except WorkflowException:
                pass
        polgroup_intid = self.intids.getId(getattr(self.polgroups_folder, polgroup_id))
        self._polgroups_intids[title] = polgroup_intid
        return polgroup_intid

    def publish(self, object):
        try:
            current_state = self.portal_workflow.getStatusOf(
                        self.portal_workflow.getChainFor(object)[0], object).get('review_state')
        except AttributeError:
            # AttributeError: 'NoneType' object has no attribute 'get'
            current_state = None
        if current_state != 'published':
            try:
                self.portal_workflow.doActionFor(object, 'publish')
            except WorkflowException:
                pass
        try:
            self.intids.getId(object)
        except KeyError:
            self.intids.register(object)

    def setup(self):
        self.portal = getToolByName(self.context, 'portal_url').getPortalObject()
        self.plone_utils = getToolByName(self.context, 'plone_utils')
        self.portal_workflow = getToolByName(self.context, 'portal_workflow')
        self.settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
        self.intids = component.getUtility(IIntIds)
        self.catalog = getToolByName(self.context, 'portal_catalog')


def convert(value, attr=None, context=None):
    if isinstance(value, datetime.date):
        try:
            value = value.strftime('%Y-%m-%d')
        except ValueError:
            value = None
    elif isinstance(value, datetime.datetime):
        value = value.strftime('%Y-%m-%d %H:%M:%S')
    elif isinstance(value, plone.namedfile.file.NamedBlobFile) or \
         isinstance(value, plone.namedfile.file.NamedBlobImage) or \
         isinstance(value, plone.namedfile.file.NamedFile) or \
         isinstance(value, plone.namedfile.file.NamedImage):
        # replace it with an url
        value = context.absolute_url() + '/@@download/%s' % attr
    elif isinstance(value, z3c.relationfield.relation.RelationValue):
        if value.to_object is None:
            value = None
        else:
            value = value.to_object.id
    elif isinstance(value, list) and value:
        value = value[:]
        if isinstance(value[0], z3c.relationfield.relation.RelationValue):
            for i, v in enumerate(value):
                if v.to_object is None:
                    value[i] = None
                else:
                    value[i] = v.to_object.id
            value = [x for x in value if x]
    return value

