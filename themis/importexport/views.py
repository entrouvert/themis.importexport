import json
import time

from DateTime import DateTime

from zope import component
from zope.app.intid.interfaces import IIntIds
from zc.relation.interfaces import ICatalog
from plone.dexterity import utils
import plone.dexterity.schema
from Products.Five import BrowserView

import themis.fields

from utils import UtilityView, convert

class GenericJson(BrowserView):
    def __call__(self):
        intids = component.getUtility(IIntIds)
        catalog = component.getUtility(ICatalog)

        basename = self.context.portal_type
        schemaName = utils.portalTypeToSchemaName(basename)
        schema = getattr(plone.dexterity.schema.generated, schemaName)
        d = {}
        for attr in schema.names():
            if not hasattr(self.context, attr):
                continue
            if isinstance(schema._InterfaceClass__attrs[attr], themis.fields.PointingDocs):
                doc_intid = intids.getId(self.context)
                d[attr] = [x.from_object.getId() for x in catalog.findRelations({'to_id': doc_intid})]
            else:
                value = getattr(self.context, attr)
                d[attr] = convert(value, attr, context=self.context)
        d['id'] = self.context.id
        d['object_type'] = self.context.Type()
        d['portal_type'] = basename
        self.request.response.setHeader('Content-type', 'application/json')
        return json.dumps(d)


class ListDeputies(UtilityView):
    def __call__(self):
        self.setup()
        timestamp = self.request.form.get('timestamp')
        kw = {}
        if timestamp:
            kw['modified'] = {'query': DateTime(timestamp), 'range': 'min'}

        l = []
        for brain in self.catalog(review_state='published',
                                  portal_type='themis.datatypes.deputy', **kw):
            l.append(brain.getURL())
        self.request.response.setHeader('Content-type', 'application/json')
        return json.dumps(l)


class ListDocuments(UtilityView):
    def __call__(self):
        self.setup()
        timestamp = self.request.form.get('timestamp')
        kw = {}
        if timestamp:
            kw['modified'] = {'query': DateTime(timestamp), 'range': 'min'}

        folder_path = '/'.join(getattr(self.portal, 'documents-diffusables').getPhysicalPath())
        l = []
        for brain in self.catalog(review_state='published', path={'query': folder_path}, **kw):
            if brain.portal_type in ('Folder', 'Topic'):
                continue
            l.append(brain.getObject().absolute_url())
        self.request.response.setHeader('Content-type', 'application/json')
        return json.dumps(l)


class ListBlockedDocuments(UtilityView):
    def __call__(self):
        self.setup()
        timestamp = self.request.form.get('timestamp')
        kw = {}
        if timestamp:
            kw['modified'] = {'query': DateTime(timestamp), 'range': 'min'}

        folder_path = '/'.join(getattr(self.portal, 'documents-diffusables').getPhysicalPath())
        l = []
        for brain in self.catalog(review_state='blocked', path={'query': folder_path}, **kw):
            if brain.portal_type in ('Folder', 'Topic'):
                continue
            l.append(brain.getObject().absolute_url())
        self.request.response.setHeader('Content-type', 'application/json')
        return json.dumps(l)



class ListCommissions(UtilityView):
    def __call__(self):
        self.setup()
        l = []
        for object in self.commissions_folder.objectValues():
            if object.portal_type != 'themis.datatypes.commission':
                continue
            if not object.members:
                continue
            l.append(object.absolute_url())
        self.request.response.setHeader('Content-type', 'application/json')
        return json.dumps(l)


class Pdb(BrowserView):
    def __call__(self):
        import pdb; pdb.set_trace()

